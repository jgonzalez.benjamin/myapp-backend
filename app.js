// Requires 
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

// Import routes 
const appRoutes = require('./routes/index');
const userRoutes = require('./routes/user');
const loginRoutes = require('./routes/login'); 

// Conection to DB 
mongoose.connection.openUri('mongodb://localhost:27017/myappDB',
    (err,res)=>{
        if (err) throw err;
        console.log('DB: \x1b[32m%s\x1b[0m','running');
    }
);

// Initialize var's
var app = express();

// Body-parser (parse application/x-www-form-urlencoded)
app.use(bodyParser.urlencoded({ extended: false } ));
//parse application/json
app.use(bodyParser.json());


// Routes 
app.use('/users', userRoutes);
app.use('/login', loginRoutes);
app.use('/', appRoutes);


// Listen requests 
app.listen(3000, () => {
    console.log('Express server port:3000 \x1b[32m%s\x1b[0m','online');
});
