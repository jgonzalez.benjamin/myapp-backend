const jwt = require('jsonwebtoken');
const SEED = require('../config/config').SEED;


//================================================================
// Token verification
//================================================================
exports.verifyToken = function(req,res,next){
    var token = req.query.token;
    jwt.verify( token, SEED, (err, cb)=>{
        if ( err ) {
            console.log( err.message );
            return res.status(401).json({
                ok: false,
                message: 'Unauthorize or incorrect token',
                errors: err
            });
        }   
        req.user = cb.user;
        next();
    });     
}
