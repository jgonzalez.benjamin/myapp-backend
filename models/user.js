var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var rolesAllowed = { 
    values: ['ADMIN_ROLE','USER_ROLE'],
    message: '{VALUE} isn t  a validate role'
}

var Schema = mongoose.Schema;
var userSchema = new Schema({
    name: { type:String,unique:true, required:[true,'Necessary name']},
    email: { type:String, unique:true, required:[true,'Necessary email']},
    password: { type:String, required:[true,'Necessary password']},
    image: { type:String, required:false},
    role: { type:String, required:true, default: 'USER_ROLE', enum: rolesAllowed },
});

// userSchema.plugin( uniqueValidator, { message: 'Duplicate {PATH}'} ); 
// This is a way to improve error messages


module.exports = mongoose.model('User', userSchema);