// Requirements
const express =  require('express');

// Initializing
var app = express();

// Routes
app.get('/', (req, res, next) => {
    res.status(200).json({
        ok:true,
        message: 'Correctly request',
    });
});

// Export route
module.exports = app;