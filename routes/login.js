// Requirements
const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../models/user');
const SEED = require('../config/config').SEED;

// Initializing
var app = express(); 

//================================================================
// Login user
//================================================================
app.post('/', (req, res)=>{
    const body = req.body;
    User.findOne({email:body.email}, (err,cb)=>{
        if( err ){
            console.log(err.message);
            return res.status(500).json({
                ok: false,
                message: err.message,
                errors: err
            });
        }
        if( !cb ){
            return res.status(400).json({
                ok: false,
                message: 'Credentials incorrect',
                errors: {message: 'Credentials incorrect - email'}
            });
        }
        if( !bcrypt.compareSync(body.password, cb.password) ){
            return res.status(400).json({
                ok: false,
                message: 'Credentials incorrect - password ',
                errors: {message: 'Credentials incorrect - password'}
            });
        }
        cb.password = ':)';
        // Create a JWT
        var token = jwt.sign({user: cb},SEED,{expiresIn: 14400});//A 4 Hours
        res.status(200).json({
            ok: true,
            message: 'Welcome '+ cb.name +'!',
            user: cb,
            id: cb._id,
            token: token
        });
    });
});

module.exports = app;