// Requirements
const express =  require('express');
const User = require('../models/user');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const mdAuth = require('../middlewares/authentication');

// Initializing
var app = express();

// Routes

//================================================================
// Get all the users
//================================================================
app.get('/', (req, res, next) => {
    User.find({ }, 'name email password="md5" image role ').exec( (err,cb)=>{
        if ( err ) {
            console.log( err.message );
            return res.status(500).json({
                ok:false,
                message: 'Error loading users',
                errors: err
            });
        }
        res.status(200).json({
            ok:true,
            message: 'Users!',
            users: cb
        });   
    }); 
});

//================================================================
// Create a user
//================================================================
app.post('/', mdAuth.verifyToken, (req, res) => {
    const body = req.body; // It works if and only if we have the body-parser
    var user = new User({
        name: body.name,
        email: body.email,
        password: bcrypt.hashSync(body.password,10),
        image: body.image,
        role: body.role
    });
    user.save((err,cb)=>{
        if ( err ) {
            console.log( err.message );
            return res.status(400).json({
                ok: false,
                message: 'Error creating a new user',
                errors: err
            });
        }
        res.status(201).json({
            ok: true,
            message: 'New user has been created',
            user: cb
        });
    });
});

//================================================================
// Update user
//================================================================
app.put('/:id', mdAuth.verifyToken, (req,res)=>{
    var _id = req.params.id;
    User.findById( _id, (err,cb)=>{
        if( err ) {
            console.log( err.message );
            return res.status(500).json({
                ok: false,
                message: 'Error searching user',
                errors: err
            });
        }
        if( !cb ){
            res.status(400).json({
                ok: false,
                message: 'The user with the id: '+_id+' does not exist',
                errors: { message: 'There is no user with that ID'}
            });
        }
        const body = req.body; 

        cb.name = body.name;
        cb.email = body.email;
        cb.role = body.role;

        cb.save( (err,userSaved)=>{
            if( err ) {
                console.log( err.message );
                return res.status(400).json({
                    ok: false,
                    message: 'Error updating user',
                    errors: err
                });
            }
            userSaved.password = ':)';
            res.status(201).json({
                ok:true,
                message: 'User updated correctly',
                user: userSaved
            });
        });
    });
});

//================================================================
// Delete user by id
//================================================================
app.delete('/:id', mdAuth.verifyToken, (req,res,next)=>{
    var _id = req.params.id;
    User.findByIdAndRemove(_id, (err,cb)=>{
        if( err ) {
            console.log( err.message );
            return res.status(500).json({
                ok: false,
                message: 'Error delete user',
                errors: err
            });
        }
        if( !cb ){
            res.status(400).json({
                ok: false,
                message: 'The user with the id: '+_id+' does not exist',
                errors: { message: 'There is no user with that ID'}
            });
        }
        res.status(200).json({
            ok:true,
            message: 'User delete correctly',
            user: cb
        });
    })
});

// Export route
module.exports = app;
